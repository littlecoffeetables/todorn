import Immutable from "seamless-immutable";

import * as types from '../actions/actionTypes';
import {
  cancelTaskNotifications,
  makeNewTask,
  makeNewTemplate,
  makeTaskFromPhantom,
  makeTaskFromTemplate,
  makeStrippedTask,
  makeUpdatedTask,
  makeUpdatedTemplate,
  setTaskNotifications,
  validateTask,
  validateTemplate,
  makeUpdatedCategory,
  makeNewCategory,
  validateCategory,
} from '../utils/taskHelpers';
import { TASK_STATUS } from "../constants/enums";


const initialState = Immutable({
  tasks: {},
  [TASK_STATUS.open]: [],
  [TASK_STATUS.closed]: [],
  [TASK_STATUS.done]: [],
  templates: {},
  categories: {},
});

export default taskReducer = (state = initialState, action) => {
  switch(action.type) {

    case types.CREATE_NEW_TASK: {
      const newTask = makeNewTask();
      validateTask(newTask);
      const newState = {
        tasks: { ...state.tasks, [newTask.id]: newTask },
        [newTask.status]: [ ...state[newTask.status], newTask.id ],
      };
      return Immutable({ ...state, ...newState });
    }

    case types.UPDATE_TASK: {
      const { task, changes } = action;
      let oldTask = state.tasks[task.id];
      let oldStatus;
      let template;
      if (oldTask) {
        oldStatus = oldTask.status;
      }
      else if (task.templateId) {
        // Upgrade a phantom task to a proper one
        oldTask = makeTaskFromPhantom(task);
        template = state.templates[task.templateId];
      }
      else {
        throw 'Trying to update a task which is not real and has no template!';
      }
      const newTask = makeUpdatedTask(oldTask, changes);
      validateTask(newTask, template);
      const newState = {
        tasks: { ...state.tasks, [newTask.id]: newTask },
      }
      if (template && task.days) {
        const newTemplate = { ...template, taskIds: { ...template.taskIds, [task.days]: newTask.id } };
        validateTemplate(newTemplate);
        newState.templates = { ...state.templates, [template.id]: newTemplate };
      }
      if (changes.status && oldStatus && changes.status !== oldStatus) {
        newState[oldStatus] = state[oldStatus].filter(tid => tid !== newTask.id);
        newState[changes.status] = [ ...state[changes.status], newTask.id ];
      }
      setTaskNotifications(newTask);
      return Immutable({ ...state, ...newState });
    }

    case types.DELETE_TASK: {
      const { task } = action;
      const status = task.status;
      const newState = {
        tasks: { ...state.tasks },
        [status]: state[status].filter(tid => tid !== task.id),
      }
      delete newState.tasks[task.id];
      // Check if template needs to be deleted
      if (task.templateId) {
        const template = state.templates[task.templateId];
        if (!template) {
          throw 'Template does not exist!';
        }
        const needsDelete = !Object.values(template.taskIds).some(otherTaskId => newState.tasks[otherTaskId]);
        if (needsDelete) {
          newState.templates = { ...state.templates };
          delete newState.templates[task.templateId];
        }
      }
      cancelTaskNotifications(task);
      return Immutable({ ...state, ...newState });
    }

    case types.CREATE_TASK_FROM_TEMPLATE: {
      const { templateId, days, changes } = action;
      const template = state.templates[templateId];
      const newTask = makeTaskFromTemplate(template, days, changes);
      const newTemplate = { ...template };
      newTemplate.taskIds = { ...template.taskIds, [days]: newTask.id };
      validateTask(newTask, newTemplate);
      validateTemplate(newTemplate);
      const newState = {
        tasks: { ...state.tasks, [newTask.id]: newTask },
        templates: { ...state.templates, [templateId]: newTemplate },
        [TASK_STATUS.open]: [ ...state[TASK_STATUS.open], newTask.id ],
      }
      setTaskNotifications(newTask);
      return Immutable({ ...state, ...newState });
    }


    case types.CREATE_NEW_TEMPLATE: {
      const { task, repeatPattern, repeatUntil } = action;
      const newTemplate = makeNewTemplate(task, repeatPattern, repeatUntil);
      const newTask = makeStrippedTask(task, { templateId: newTemplate.id, days: 0 });
      validateTemplate(newTemplate);
      validateTask(newTask, newTemplate);
      const newState = {
        templates: { ...state.templates, [newTemplate.id]: newTemplate },
        tasks: { ...state.tasks, [task.id]: newTask },
      };
      return Immutable({ ...state, ...newState });
    }

    case types.UPDATE_TEMPLATE: {
      const { templateId, task, changes, stripTask } = action;
      const template = state.templates[templateId];
      // console.log(template, task, changes)
      const newTemplate = makeUpdatedTemplate(template, changes);
      validateTemplate(newTemplate);
      const newState = {
        templates: { ...state.templates, [templateId]: newTemplate },
      }
      if (stripTask) {
        const newTask = makeStrippedTask(task, { templateId: task.templateId, days: task.days, doneAt: task.doneAt });
        validateTask(newTask, newTemplate);
        newState.tasks = { ...state.tasks, [task.id]: newTask }
      }
      if (changes.startTime && changes.startTime !== template.startTime) {
        // TODO: notifications for template-generated tasks?
        // setTaskNotifications( task );
      }
      return Immutable({ ...state, ...newState });
    }

    // Instead of deleting templates, update their repeatPattern to null ?
    // case types.DELETE_TEMPLATE: {
    //   const { templateId } = action;
    //   const template = state.templates[templateId];
    //   const newState = {
    //     templates: { ...state.templates },
    //     tasks: { ...state.tasks },
    //   }
    //   for (const taskId of Object.values(template.taskIds)) {
    //     newState.tasks[taskId].templateId = null;
    //   }
    //   // TODO: notifications for template-generated tasks
    //   // cancelTaskNotifications(task);
    //   delete newState.templates[templateId];
    //   return Immutable({ ...state, ...newState });
    // }

    case types.CREATE_OR_UPDATE_CATEGORY: {
      const { changes } = action;
      const newState = {};
      let newCategory;
      if (changes.id) {
        const oldCategory = state.categories[changes.id];
        newCategory = makeUpdatedCategory(oldCategory, changes);
      } else {
        newCategory = makeNewCategory(changes);
      }
      validateCategory(newCategory);
      newState.categories = { ...(state.categories ?? {}), [newCategory.id]: newCategory };
      return Immutable({ ...state, ...newState });
    }

    default:
      return state;
  }
};