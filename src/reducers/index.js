import { combineReducers } from 'redux';
import task from './taskReducer';
import tmp from './tmpReducer';
import settings from './settingsReducer';

const rootReducer = combineReducers({
  task,
  tmp,
  settings,
});

export default rootReducer;