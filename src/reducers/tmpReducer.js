import Immutable from "seamless-immutable";

import * as types from '../actions/actionTypes';
import {
  cancelTaskNotifications,
  setTaskNotifications,
  validateTask,
} from '../utils/taskHelpers';
import { TASK_STATUS } from "../constants/enums";


const initialState = Immutable({
  phantomTasks: {},
});

export default tmpReducer = (state = initialState, action) => {
  switch(action.type) {
    case types.CREATE_PHANTOM_TASKS: {
      const newTasks = {};
      action.tasks.forEach(t => {
        const template = action.templates[t.templateId];
        validateTask(t, template);
        newTasks[t.id] = t;
      });
      const newState = {
        phantomTasks: { ...state.phantomTasks, ...newTasks },
      };
      return Immutable({ ...state, ...newState });
    }
    // case types.DELETE_PHANTOM_TASK: {
    //   const { taskId } = action;
    //   const task = state.phantomTasks[taskId];
    //   const newState = {
    //     phantomTasks: { ...state.phantomTasks },
    //   }
    //   cancelTaskNotifications(task);
    //   delete newState.phantomTasks[taskId];
    //   return Immutable({ ...state, ...newState });
    // }
    case types.CLEAR_PHANTOM_TASKS: {
      return Immutable({ ...state, phantomTasks: {} });
    }
    default:
      return state;
  }
};