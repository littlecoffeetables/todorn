import Immutable from "seamless-immutable";

import * as types from '../actions/actionTypes';


const initialState = Immutable({
  debugMode: false,
});

export default tmpReducer = (state = initialState, action) => {
  switch(action.type) {
    case types.SET_SETTING: {
      const { setting, value } = action;
      return Immutable({ ...state, [setting]: value });
    }
    default:
      return state;
  }
};