import { encode as btoa } from 'base-64';
import { format, isSameDay, isToday, parseJSON } from 'date-fns';
import { strToTime } from './timeHelpers';


export const randomString = (len = 8) => {
  // return btoa(+new Date()).slice(-10, -2);
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-_';
  return Array(len).fill(null).map(_ => chars[Math.floor(Math.random() * chars.length)]).join('');
}


export const formatDatetime = (datetime, default_ = undefined) => {
  if (datetime === 'null') datetime = null;
  if (!datetime) return default_ || '???';
  datetime = strToTime(datetime);
  const f = isToday(datetime) ? 'HH:mm' : 'd.M.y HH:mm';
  return format(datetime, f);
}


export const formatRange = (start, end) => {
  const startFormat = isToday(start) ? 'HH:mm' : 'd.M.y HH:mm';
  const endFormat = isSameDay(start, end) ? 'HH:mm' : 'd.M.y HH:mm';
  return `${format(start, startFormat)} - ${format(end, endFormat)}`;
}

