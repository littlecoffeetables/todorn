import { parseJSON } from "date-fns";


export const timeToStr = (time) => {
  if (typeof time === 'string' || !time) return time;
  return JSON.stringify(time);
}

export const strToTime = (str) => {
  if (typeof str !== 'string') return str;
  if (str === 'null') return null;
  return parseJSON(str);
}


export const timeDeltaToMinutes = (delta) => {
  return delta / 60000;
}

export const minutesToDays = (minutes) => {
  return minutes / 60 / 24;
}

export const minutesToDelta = (minutes) => {
  return minutes * 60000;
}

