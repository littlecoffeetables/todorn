import Immutable from "seamless-immutable";
import {
  add,
  differenceInCalendarWeeks,
  differenceInMonths,
  format,
  getDate,
  getDay,
  isValid,
} from "date-fns";

import { randomString } from "./stringHelpers";
import { TASK_STATUS, PATTERN_MODE } from '../constants/enums';
import { cancelNotification, scheduleNotification } from "./notifications";
import {
  minutesToDays,
  strToTime,
  timeDeltaToMinutes,
  timeToStr,
} from "./timeHelpers";
import { DAYS_PER_MONTH } from "../constants/calendar";
import { ValidationError } from "./errors";


/*
  Required task fields:
  - id: random string identifier
  - status
  - title: user given name (may be missing if templateId is present)
  Other possible task fields:
  - startTime: user given start time (timestamp string)
  - duration: user given duration in minutes (integer) (startTime + duration == endTime if flexible == false)
  - minDuration: user given estimated minimum duration in minutes (task can be shortened to this duration if necessary to avoid delaying it)
  - deadline: user given deadline (timestamp string)
  - flexible: false if task is an event that should be done at a specific time
  - spoons: user given estimate of how exhausting the task is (integer)
  - doneAt: timestamp string representing when the task was marked as done
  - templateId: identifier of a task template which this task is an instance of
  - days: required if templateId is present. Integer representing the number of days this task originally was offset from the template
*/

const TASK_FIELDS = {
  id: { required: true, type: 'string' },
  status: { required: true, oneOf: Object.values(TASK_STATUS) },
  title: { softRequired: true, type: 'string' },

  startTime: { type: 'string', time: true },
  duration: { type: 'number', integer: true, min: 0 },
  minDuration: { type: 'number', integer: true, min: 0 },
  deadline: { type: 'string', time: true },
  flexible: { type: 'boolean' },
  spoons: { type: 'number', integer: true },
  categoryIds: { type: 'object', array: true },

  doneAt: { type: 'string', time: true },
  templateId: { type: 'string', requires: 'days' },
  days: { type: 'number', requires: 'templateId' },
};


/*
  Required template fields:
  - id: random string identifier
  - startTime: same as on task, this will be offset for each instance
  - repeatPattern: an object describing how to create new instances (null if "deleted")
  - repeatUntil: timestamp string
  - taskIds: object of identifiers of instance tasks of this template, with their original offset in days as the key
  - title: same as on task, default title for each instance
  Other possible template fields:
  - duration, minDuration, flexible, spoons: same as on task, these will be copied to each instance
  - deadline: same as on task, this will be offset for each instance

  Note:
  - New instances by default do not carry any data, so they will not be saved as new tasks.
    Instead, their data is dynamically fetched from the instance when necessary.
    However they can be individually edited at which point they will be created as their own tasks.
  - Only tasks have status, doneAt and templateId.
  - Only templates have taskIds, repeatPattern and repeatUntil.
*/

const TEMPLATE_FIELDS = {
  id: { required: true, type: 'string' },
  title: { required: true, type: 'string' },
  taskIds: { required: true, type: 'object' },
  startTime: { required: true, type: 'string', time: true },
  repeatPattern: { type: 'object' },
  repeatUntil: { type: 'string', time: true },

  duration: { type: 'number', integer: true, min: 0 },
  minDuration: { type: 'number', integer: true, min: 0 },
  deadline: { type: 'string', time: true },
  flexible: { type: 'boolean' },
  spoons: { type: 'number', integer: true },
  categoryIds: { type: 'object', array: true },
};


export const ABSOLUTE_TEMPLATE_FIELDS = [
  'title',
  'duration',
  'minDuration',
  'flexible',
  'spoons',
  'categoryIds',
];

export const RELATIVE_TEMPLATE_FIELDS = [
  'startTime',
  'deadline',
];


/*
  Required category fields:
  - id: random string identifier
  - name: user given name
  - color: color string
  Other possible category fields:
  - icon (TODO): MaterialCommunityIcons name
  - deleted: true if hidden from lists
*/

const CATEGORY_FIELDS = {
  id: { required: true, type: 'string' },
  name: { required: true, type: 'string' },
  color: { required: true, type: 'string' },

  icon: { type: 'string' },
  deleted: { type: 'boolean' },
};



export const validateTask = (rawTask, template) => {
  const task = getTaskFields(rawTask, template);
  return validateTaskOrTemplate(task, TASK_FIELDS, 'Task', rawTask);
}

export const validateTemplate = (template) => {
  return validateTaskOrTemplate(template, TEMPLATE_FIELDS, 'Template', template);
}

export const validateCategory = (category) => {
  return validateTaskOrTemplate(category, CATEGORY_FIELDS, 'Category', category);
}

const validateTaskOrTemplate = (t, fields, tt, rawTask) => {
  Object.entries(fields).forEach(([f, v]) => {
    if (v.required && !rawTask[f]) throw new ValidationError(`${tt} missing required field ${f}: ${t}`);
    if (v.softRequired && !t[f]) throw new ValidationError(`${tt} missing soft-required field ${f}: ${t}`);
    if (t[f] === undefined || t[f] === null) return; // Field is empty, stop validation
    if (v.type && typeof t[f] !== v.type) throw new ValidationError(`${tt} field ${f} is wrong type: ${t}`);
    if (v.time && !isValid(strToTime(t[f]))) throw new ValidationError(`${tt} has invalid time field ${f}: ${t}`);
    if (v.array && typeof t[f].length !== 'number') throw new ValidationError(`${tt} field ${f} is not an array: ${t}`);
    if (v.integer && parseInt(t[f]) !== t[f]) throw new ValidationError(`${tt} field ${f} is not an integer: ${t}`);
    if (v.min !== undefined && t[f] < v.min) throw new ValidationError(`${tt} field ${f} is too small: ${t}`);
    if (v.requires && (t[v.requires] === undefined || t[v.requires] === null)) {
      throw new ValidationError(`${tt} field ${f} is present but dependent field ${v.requires} is not: ${t}`);
    }
    if (v.oneOf && v.oneOf.indexOf(t[f]) < 0) throw new ValidationError(`${tt} field ${f} is not an allowed value: ${t}`);
    if (t[f] !== t[f]) throw new ValidationError(`${tt} field ${f} is NaN: ${t}`);
  });
  Object.keys(t).forEach(f => {
    if (fields[f] === undefined) throw new ValidationError(`${tt} has illegal field ${f}: ${t}`);
  })
  return true;
}


// TODO: task & template edit history
// TODO: notifications for phantom tasks
// TODO: reschedule template when instance is completed: no / to the day / to the minute
// TODO: configurable daily cutoff


export const makeNewTask = () => {
  return Immutable({
    id: randomString(9),
    status: TASK_STATUS.open,
    title: 'New task',
  });
}


export const makeUpdatedTask = (task, changes) => {
  return Immutable({ ...task, ...changes });
}


// Delete fields which are getting moved to a template from task
export const makeStrippedTask = (task, changes) => {
  const { id, status } = task;
  return Immutable({
    id,
    status,
    ...changes,
  })
}


export const makeTaskFromTemplate = (template, days, changes) => {
  const task = {
    id: randomString(10),
    status: TASK_STATUS.open,
    templateId: template.id,
    days,
  };
  ABSOLUTE_TEMPLATE_FIELDS.forEach(f => {
    task[f] = template[f];
  });
  RELATIVE_TEMPLATE_FIELDS.forEach(f => {
    if (template[f]) task[f] = timeToStr(add(strToTime(template[f]), { days }));
  });
  return Immutable({...task, ...changes});
}


export const makePhantomTask = (template, days) => {
  const id = `phantom-${template.id}-${days}`;
  return Immutable({
    id: id,
    status: TASK_STATUS.open,
    templateId: template.id,
    days,
  });
}


// Dynamically get data for phantom- or otherwise incomplete tasks from their template when necessary.
export const getTaskFields = (task, template) => {
  if (!template) return task;

  const { days } = task;
  if (days === undefined) {
    console.log(task, template);
    throw 'Missing "days" field in template-based task!';
  }

  const fields = {};
  ABSOLUTE_TEMPLATE_FIELDS.forEach(f => {
    fields[f] = template[f];
  });
  RELATIVE_TEMPLATE_FIELDS.forEach(f => {
    if (template[f]) fields[f] = timeToStr(add(strToTime(template[f]), { days }));
  })

  return Immutable({ ...fields, ...(task || {}) });
}


export const makeTaskFromPhantom = (phantomTask) => {
  return Immutable({
    ...phantomTask,
    id: randomString(11),
  })
}


export const isOpen = (task) => {
  return task.status === TASK_STATUS.open;
}


export const makeNewTemplate = (task, repeatPattern, repeatUntil) => {
  const template = {
    id: randomString(8),
    repeatPattern,
    repeatUntil,
    taskIds: { 0: task.id },
  };
  ABSOLUTE_TEMPLATE_FIELDS.forEach(f => {
    template[f] = task[f];
  });
  RELATIVE_TEMPLATE_FIELDS.forEach(f => {
    template[f] = task[f];
  });
  return Immutable(template);
}


export const makeUpdatedTemplate = (template, changes) => {
  return Immutable({ ...template, ...changes });
}


export const getOpenTasksInTimeRange = (templates, tasks, openTasks, minTime, maxTime, exclusive=false) => {
  const taskList = [];
  const getEndTime = (task) => add(strToTime(task.startTime), {minutes: task.duration ?? 0});

  for (const taskId of openTasks) {
    const rawTask = tasks[taskId];
    const template = templates[rawTask.templateId];
    const task = getTaskFields(rawTask, template);
    if (!task.startTime) {
      const deadline = task.deadline && strToTime(task.deadline);
      if (!exclusive || (deadline && deadline >= minTime && deadline <= maxTime)) {
        taskList.push(task);
      }
      continue;
    }
    if (strToTime(task.startTime) > maxTime) continue;
    if ((!task.flexible || exclusive) && getEndTime(task) < minTime) continue;
    taskList.push(task);
  }

  for (const template of Object.values(templates)) {
    const start = strToTime(template.startTime);
    const effectiveMin = exclusive ? minTime : (minTime - minutesToDays(template.duration ?? 0));
    const effectiveMax = Math.min(maxTime, strToTime(template.repeatUntil) ?? Infinity)
    const startDays = Math.ceil(minutesToDays(timeDeltaToMinutes( effectiveMin - start )));
    const endDays = Math.floor(minutesToDays(timeDeltaToMinutes( effectiveMax - start )));
    const minDays = exclusive ? Math.max(startDays, 0) : startDays;
    for (let days = minDays; days <= endDays; ++days) {
      if (!template.taskIds) {
        console.log(template)
        throw 'Template missing taskIds field!';
      }
      // task exists on its own, skip it here
      if (template.taskIds[days]) {
        continue;
      }
      if (templateHasInstanceOn(template, days)) {
        const phantomTask = makePhantomTask(template, days);
        taskList.push(phantomTask);
      }
    }
  }
  // TODO: trim flexible non-phantom template instances that are well overdue... or reschedule all later instances around it?
  //  Note: this would not apply to templates that won't be rescheduled when an instance is completed.

  return taskList;
}


export const templateHasInstanceOn = (template, days) => {
  const pattern = template.repeatPattern;
  const start = strToTime(template.startTime);
  const time = add(start, { days });
  switch (pattern.mode) {
    case PATTERN_MODE.days: {
      return days % pattern.N_days === 0;
    }
    case PATTERN_MODE.months: {
      const months = differenceInMonths(time, start);
      return time === add(start, { months });
    }
    case PATTERN_MODE.weekly: {
      // Pattern starts from the monday of the week of the first instance so count weeks passed since
      const weeks = differenceInCalendarWeeks(time, start, { weekStartsOn: 1 }) % pattern.N_weekly;
      const weekdayOffset = (getDay(start) + 6) % 7; // monday = 0
      const i = weeks * 7 + (days + weekdayOffset) % (pattern.N_weekly * 7);
      return pattern.weekly[i];
    }
    case PATTERN_MODE.monthly: {
      const date = getDate(time);
      if (pattern.monthly[date - 1]) return true;
      if (pattern.reverseMonthly[DAYS_PER_MONTH - date]) return true;
      return false;
    }
    default:
      return false;
  }
}



export const setTaskNotifications = (task) => {
  // console.log(task);
  if (task.status === TASK_STATUS.open) {
    scheduleNotification(task.deadline, task.title, 'Deadline passed!', `deadline-${task.id}`);
    scheduleNotification(task.startTime, task.title, 'Planned start now!', `startTime-${task.id}`);
  } else {
    cancelTaskNotifications(task);
  }
}

export const cancelTaskNotifications = (task) => {
  cancelNotification(`deadline-${task.id}`);
  cancelNotification(`startTime-${task.id}`);
}


export const validateRepeatPattern = (pattern) => {
  switch (pattern.mode) {
    case PATTERN_MODE.days: {
      return (pattern.N_days ?? 1) >= 1 && (pattern.N_days ?? 1) < 9999;
    }
    case PATTERN_MODE.months: {
      return (pattern.N_months ?? 1) >= 1 && (pattern.N_months ?? 1) < 99;
    }
    case PATTERN_MODE.weekly: {
      return (pattern.N_weekly ?? 1) >= 1 && (pattern.N_weekly ?? 1) <= 10;
    }
    case PATTERN_MODE.monthly: {
      return true;
    }
    case null: {
      return true;
    }
    default:
      return false;
  }
}

export const trimRepeatPattern = (pattern) => {
  const to_ret = { mode: pattern.mode };
  switch (pattern.mode) {
    case PATTERN_MODE.days: {
      to_ret.N_days = pattern.N_days ?? 1;
      break;
    }
    case PATTERN_MODE.months: {
      to_ret.N_months = pattern.N_months ?? 1;
      break;
    }
    case PATTERN_MODE.weekly: {
      to_ret.N_weekly = pattern.N_weekly ?? 1;
      to_ret.weekly = pattern.weekly ? pattern.weekly.slice(0, to_ret.N_weekly * 7) : [];
      break;
    }
    case PATTERN_MODE.monthly: {
      to_ret.monthly = pattern.monthly ?? [];
      to_ret.reverseMonthly = pattern.reverseMonthly ?? [];
      break;
    }
  }
  return to_ret;
}

export const getRepeatPatternDescription = (pattern) => {
  switch (pattern.mode) {
    case PATTERN_MODE.days: {
      return `Repeats every ${pattern.N_days} days`;
    }
    case PATTERN_MODE.months: {
      return `Repeats every ${pattern.N_months} months`;
    }
    case PATTERN_MODE.weekly: {
      return `${pattern.N_weekly}-weekly schedule`;
    }
    case PATTERN_MODE.monthly: {
      return 'Monthly schedule';
    }
    case null: {
      return 'Not set';
    }
    default:
      return '???';
  }
}


export const makeNewCategory = (changes) => {
  const { name, color } = changes;
  return Immutable({
    id: randomString(7),
    name,
    color,
    // TODO: icon
  });
}

export const makeUpdatedCategory = (category, changes) => {
  return Immutable({ ...category, ...changes });
}

