import notifee, { TimestampTrigger, TriggerType } from '@notifee/react-native';
import { format } from 'date-fns';

import { strToTime } from './timeHelpers';


const prepData = (id, title, body, channelId) => {
  const data = {
    title: title,
    body: body,
    android: {
      channelId,
    },
  };
  if (id) data.id = id;
  return data;
}


export const sendNotificationImmediately = async (title, body, id) => {
  const channelId = await notifee.createChannel({
    id: 'test',
    name: 'Test Channel',
  });

  const data = prepData(id, title, body, channelId);

  try {
    await notifee.displayNotification(data);
  } catch (e) {
    console.log(e);
  }
}


export const dumbScheduleNotification = async (datetime, title, body, id) => {
  const channelId = await notifee.createChannel({
    id: 'test',
    name: 'Test Channel',
  });

  const trigger: TimestampTrigger = {
    type: TriggerType.TIMESTAMP,
    timestamp: datetime.getTime(),
  };

  const data = prepData(id, title, body, channelId);

  try {
    await notifee.createTriggerNotification(data, trigger);
  } catch (e) {
    console.log(e);
  }
}


export const cancelNotification = async (id) => {
  await notifee.cancelNotification(id);
}


export const scheduleNotification = async (datetime, title, body, id) => {
  if (typeof datetime === 'string') datetime = strToTime(datetime);
  if (!datetime) {
    if (id) await notifee.cancelNotification(id);
    return;
  }

  body = `${body} ${format(datetime, 'd.M.y HH:mm')}`;

  const channelId = await notifee.createChannel({
    id: 'test',
    name: 'Test Channel',
  });

  const trigger: TimestampTrigger = {
    type: TriggerType.TIMESTAMP,
    timestamp: datetime.getTime(),
  };

  const data = prepData(id, title, body, channelId);

  if (datetime > new Date()) {
    try {
      await notifee.createTriggerNotification(data, trigger);
    } catch (e) {
      console.log(e);
      if (id) await notifee.cancelNotification(id);
    }
  }
  else {
    if (id) await notifee.cancelNotification(id);
  }
}


export const listScheduledNotifications = async (cb) => {
  const result = await notifee.getTriggerNotifications();
  cb(result);
}
