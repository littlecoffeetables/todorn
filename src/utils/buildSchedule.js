import { differenceInMinutes } from 'date-fns';
import add from 'date-fns/add';
import Immutable from 'seamless-immutable';

import { strToTime, timeDeltaToMinutes } from '../utils/timeHelpers';
import { getTaskFields } from './taskHelpers';


const buildSchedule = (taskList, templates) => {
  const schedule = [];
  const remainingTasks = [];
  const now = new Date();

  taskList.forEach(rawTask => {
    const task = getTaskFields(rawTask, templates[rawTask.templateId]);
    // rigid tasks must be included in the schedule at their designated times
    // TODO: require that rigid tasks have a start and end time
    // (for now treat ones without start time as flexible and ones without end time as lasting 1 hour)
    if (!task.flexible && task.startTime) {
      schedule.push({ taskId: task.id, start: strToTime(task.startTime), end: estimateEndTime(task) });
      return;
    }
    else {
      remainingTasks.push(task);
    }
  });
  schedule.sort((a, b) => {
    return a.start - b.start;
  });

  // Now that we have the schedule containing all upcoming rigid tasks in order of occurrence, we can start filling the gaps
  // We should start by fitting in tasks that are important and long
  // TODO: split into categories and pick from the category with the highest ratio of available to achieved+scheduled points
  // TODO: Don't exceed spoon estimate
  // Note: rigid tasks may be in the past if not yet marked as done

  // Step 1. Calculate times inbetween each task scheduled thus far
  const originalGaps = calculateMinutesInbetween(schedule, now);
  const gaps = originalGaps.map(m => ({ minutes: m.minutes, start: m.start, scheduled: [], scheduledFromEnd: [] }));

  // Step 2. Sort the remaining tasks such that the most important are last
  remainingTasks.sort((a, b) => {
    return estimateImportance(a, now) - estimateImportance(b, now);
  });

  // Step 3. Fit all tasks in order of most important to least into the earliest timeslot where they'll fit
  //  But not before their start time.
  while (remainingTasks.length > 0) {
    const task = remainingTasks.pop();
    const taskStart = strToTime(task.startTime);
    const duration = estimateDuration(task);
    const minDuration = estimateDuration(task, true);
    let delta;
    const slot = gaps.findIndex(gap => {
      delta = taskStart ? Math.max(0, differenceInMinutes(taskStart, gap.start)) : 0;
      return gap.minutes - delta >= minDuration;
    });
    if (slot === -1) {
      const minStart = schedule.length === 0 ? now : schedule[schedule.length-1].end;
      const start = new Date(Math.max(taskStart ?? 0, minStart));
      schedule.push({ taskId: task.id, start: start, end: add(start, {minutes: duration}) });
    }
    else {
      const gap = gaps[slot];
      const actualDuration = Math.min(duration, gap.minutes);
      if (delta > 5) {
        gap.scheduledFromEnd.push({...task, actualDuration});
      } else {
        gap.scheduled.push({...task, actualDuration});
      }
      gap.minutes -= actualDuration;
    }
  }

  // Actually insert the tasks into the schedule (this is deferred to not mess up indexes)
  for (let i = gaps.length - 1; i >= 0; --i) {
    let start = i === 0 ? now : schedule[i-1].end;
    const gap = gaps[i];
    const gapScheduled = [ ...gap.scheduled, ...gap.scheduledFromEnd.slice(0).reverse() ];
    const toInsert = gapScheduled.map(task => {
      const tmpStart = new Date(Math.max(start, strToTime(task.startTime) ?? 0));
      const duration = task.actualDuration ?? estimateDuration(task);
      const end = add(tmpStart, {minutes: duration});
      start = end;
      return { taskId: task.id, start: tmpStart, end: end };
    });
    schedule.splice(i, 0, ...toInsert);
  }
  return schedule;
}


const estimateImportance = (task, now) => {
  // TODO: tasks should have user inputtable points and, later, custom point curves (esp. for repeating tasks)
  let points = 10;
  const hoursUntilDeadline = timeDeltaToMinutes(Math.max(0, strToTime(task.deadline) - now)) / 60;
  let bonusFromDeadline = task.deadline ? Math.max(0, 72 - hoursUntilDeadline) : 0;
  let durationMinutes = estimateDuration(task);
  let bonusFromDuration = durationMinutes / 10;
  let spoonCost = task.spoons || 1;
  let total = (points + bonusFromDuration) / spoonCost + bonusFromDeadline;
  return total;
}

const estimateDuration = (task, useMinimum=false) => {
  return (useMinimum ? task.minDuration : undefined) ?? task.duration ?? 60;
}

const estimateEndTime = (task) => add(strToTime(task.startTime), {minutes: estimateDuration(task)});

const calculateMinutesInbetween = (schedule, now) => {
  const gaps = [];
  let lastEnd = now;
  for (const task of schedule) {
    if (task.end < lastEnd) {
      continue;
    }
    const minutes = Math.max(0, timeDeltaToMinutes(task.start - lastEnd));
    if (minutes > 0) {
      gaps.push(Immutable({ minutes, start: lastEnd }));
    }
    lastEnd = task.end;
  }
  return gaps;
}

export default buildSchedule;