import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { IconButton, Text } from 'react-native-paper';
import { useDispatch } from 'react-redux';

import * as appActions from '../actions/index';
import { theme } from '../constants/colors';
import TemplateList from '../views/template_list';
import TemplateForm from '../views/template_form';
import Navbar from '../components/navbar';

const Stack = createNativeStackNavigator();


const MainListTab = (props) => {
  const dispatch = useDispatch();
  const createNewTask = () => {
    dispatch(appActions.createNewTask());
  }

  return <Stack.Navigator screenOptions={{ header: Navbar }}>
    <Stack.Screen
      name='Templates'
      component={TemplateList}
      options={{
        style: {backgroundColor: theme.colors.background},
        title: 'All Repetition Templates',
      }}
    />
    <Stack.Screen
      name='Edit Template'
      component={TemplateForm}
    />
  </Stack.Navigator>
};

export default MainListTab;