import React, { useState } from "react";
import { ScrollView, View } from "react-native";
import {
  Button,
  Dialog,
  IconButton,
  List,
  Portal,
  TextInput,
} from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import Immutable from "seamless-immutable";

import * as appActions from "../actions";
import NewTaskButton from "../components/new_task_button";


const CategoryListScreen = (props) => {
  const { categories, debugMode } = useSelector(({task, settings}) => {
    const { debugMode } = settings;
    return { categories: task.categories || [], debugMode };
  });
  const dispatch = useDispatch();
  const [shownCategory, setShownCategory] = useState(null);
  const [edits, setEdits] = useState(null);

  const categoryPressed = (category) => {
    setShownCategory(category);
  }

  const newCategory = () => setShownCategory({});

  const cancelEdit = () => {
    setShownCategory(null);
    setEdits(null);
  }

  const save = () => {
    if (edits) {
      dispatch(appActions.createOrUpdateCategory(edits));
      setShownCategory(null);
      setEdits(null);
    }
  }

  const deleteCategory = (category) => {
    dispatch(appActions.createOrUpdateCategory({ id: category.id, deleted: true }));
  }

  const editProp = (prop, newValue) => {
    setEdits(Immutable({ ...shownCategory, ...edits, [prop]: newValue }));
  }

  return <>
    <ScrollView>
      <Portal>
        <Dialog visible={shownCategory} onDismiss={cancelEdit}>
          <Dialog.Title>Edit category {shownCategory?.name}</Dialog.Title>
          <Dialog.Content>
            <TextInput
              label='Name'
              value={(edits ?? shownCategory)?.name}
              onChangeText={newName => editProp('name', newName)}
              />
            <TextInput
              label={'Color (hex without #)'}
              value={(edits ?? shownCategory)?.color}
              onChangeText={newColor => editProp('color', newColor)}
              />
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={cancelEdit}>Cancel</Button>
            <Button onPress={save}>Save</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      {Object.values(categories).map(c => {
        if (c.deleted) return null;
        return <List.Item
          key={c.id}
          onPress={() => categoryPressed(c)}
          style={{ backgroundColor: `#${c.color}` }}
          title={`${c.name}${debugMode ? ` [${c.id}]` : ''}`}
          right={() => <IconButton
            icon='trash-can'
            onPress={() => deleteCategory(c)}
          />}
          />
      })}
    </ScrollView>
    <View>
      <NewTaskButton onPress={newCategory} />
    </View>
  </>
}


export default CategoryListScreen;