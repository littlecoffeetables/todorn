import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import NextUp from '../views/next_up';
import TaskForm from '../views/task_form';
import Navbar from '../components/navbar';

const Stack = createNativeStackNavigator();


const NextUpTab = (props) => {
  return <Stack.Navigator screenOptions={{ header: Navbar }}>
    <Stack.Screen name='NextUpList' component={NextUp} options={{title: 'Next Up'}} />
    <Stack.Screen name='Edit Task' component={TaskForm} />
  </Stack.Navigator>
};

export default NextUpTab;