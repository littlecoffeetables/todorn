import React from "react";
import { ScrollView } from "react-native";
import { List, Switch } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";

import * as appActions from "../actions";


const SettingsScreen = (props) => {
  const { navigation } = props;
  const settings = useSelector(({settings}) => {
    return settings;
  });
  const dispatch = useDispatch();

  const setSetting = (setting, value) => {
    dispatch(appActions.setSetting(setting, value));
  }

  const toggleDebugMode = () => {setSetting('debugMode', !settings.debugMode)}

  const manageCategories = () => {
    navigation.navigate('Manage Categories');
  }

  return <ScrollView>
    <List.Item
      title='Debug mode'
      onPress={toggleDebugMode}
      right={() => {
        return <Switch value={settings.debugMode} onValueChange={value => setSetting('debugMode', value)} />
      }}
      />
    <List.Item
      title='Manage categories...'
      onPress={manageCategories}
      />
  </ScrollView>
}


export default SettingsScreen;