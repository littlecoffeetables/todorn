import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import TaskForm from '../views/task_form';
import Archive from '../views/archive';
import Navbar from '../components/navbar';

const Stack = createNativeStackNavigator();


const ArchiveTab = (props) => {
  return <Stack.Navigator screenOptions={{ header: Navbar }}>
    <Stack.Screen name='Archived Tasks' component={Archive} />
    <Stack.Screen name='Edit Task' component={TaskForm} />
  </Stack.Navigator>
};

export default ArchiveTab;