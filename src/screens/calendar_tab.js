import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { IconButton, Text } from 'react-native-paper';
import { useDispatch } from 'react-redux';

import AgendaView from '../views/agenda';
import * as appActions from '../actions/index';
import { theme } from '../constants/colors';
import Navbar from '../components/navbar';

const Stack = createNativeStackNavigator();


const CalendarTab = (props) => {
  const dispatch = useDispatch();

  return <Stack.Navigator screenOptions={{ header: Navbar }}>
    <Stack.Screen
      name='Month Calendar'
      component={AgendaView}
      options={{
        style: {backgroundColor: theme.colors.background},
        title: 'Calendar',
      }}
    />
  </Stack.Navigator>
};

export default CalendarTab;