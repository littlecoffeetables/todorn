import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import TaskForm from '../views/task_form';
import { theme } from '../constants/colors';
import MainList from '../views/main_list';
import Navbar from '../components/navbar';

const Stack = createNativeStackNavigator();


const MainListTab = (props) => {
  return <Stack.Navigator screenOptions={{ header: Navbar }}>
    <Stack.Screen
      name='Tasks'
      component={MainList}
      options={{
        style: {backgroundColor: theme.colors.background},
        title: 'All Open Tasks',
      }}
    />
    <Stack.Screen
      name='Edit Task'
      component={TaskForm}
    />
  </Stack.Navigator>
};

export default MainListTab;