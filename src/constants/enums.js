
export const TASK_STATUS = {
  open: 'open',
  closed: 'closed',
  done: 'done',
}


export const PATTERN_MODE = {
  days: 'days',
  weekly: 'weekly',
  months: 'months',
  monthly: 'monthly',
}
