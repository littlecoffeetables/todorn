import { DefaultTheme } from "react-native-paper";
import { DefaultTheme as NavigationTheme } from "@react-navigation/native";


export const COLORS = {
  primary: '#e87e6f',
  alt: '#333',
  secondary: 'indigo',
  tertiary: 'teal',
  background: '#555',
  text: '#eee',
  dimText: '#aaa',
  delete: '#c33',
}



export const theme = {
  ...DefaultTheme,
  roundness: 8,
  dark: true,
  colors: {
    ...DefaultTheme.colors,
    primary: COLORS.primary,
    accent: COLORS.secondary,
    text: COLORS.text,
    background: COLORS.background,
    backdrop: '#555b',
    surface: COLORS.alt,
  },
};

export const navTheme = {
  ...NavigationTheme,
  colors: {
    ...NavigationTheme.colors,
    primary: COLORS.secondary,
    card: COLORS.primary,
    background: COLORS.background,
  }
}
