import React, { useState } from 'react';
import {
  Button,
  List,
  Switch,
  TextInput,
  Appbar,
  Text,
} from 'react-native-paper';
import {
  ScrollView,
} from 'react-native';
import { add, parseISO } from 'date-fns';
import { useBackHandler } from '@react-native-community/hooks';
import { useDispatch, useSelector } from 'react-redux';

import DateTimeInput from '../components/datetimeinput';
import DurationInput from '../components/duration_input';
import SpoonInput from '../components/spoon_input';
import * as appActions from '../actions/index';
import { strToTime, timeDeltaToMinutes, timeToStr } from '../utils/timeHelpers';
import RepeatPatternInput from '../components/repeat_pattern_input';
import CategoriesInput from '../components/categories_input';


const TemplateForm = (props) => {
  const { navigation } = props;
  const { templateId } = props.route.params;
  const { tasks, templates, debugMode } = useSelector(({task, settings}) => {
    const { tasks, templates } = task;
    const { debugMode } = settings;
    return { tasks, templates, debugMode };
  });
  const dispatch = useDispatch();

  const originalTemplate = templates[templateId];
  const [template, setTemplate] = useState(originalTemplate);

  if (!template) {
    console.log(template, templateId, templates);
    throw 'Template does not exist!';
  }

  const onSave = () => {
    const templateChanges = { ...template };
    dispatch(appActions.updateTemplate(originalTemplate.id, originalTemplate, templateChanges));
    navigation.goBack();
  }

  const onCancel = () => {
    navigation.goBack();
  }

  const updateEndTime = (datetime) => {
    const duration = timeDeltaToMinutes(strToTime(datetime) - strToTime(template.startTime));
    if (duration >= 0) {
      setTemplate({ ...template, duration });
    }
  }

  const toggleFlexible = () => {
    setTemplate({ ...template, flexible: !template.flexible });
  }

  const getEndTime = () => {
    return template.duration ? add( strToTime(template.startTime), { minutes: template.duration } ) : undefined;
  }

  const getEndTimeDefault = () => {
    return add( strToTime(template.startTime) || new Date, { minutes: template.duration || 120 } );
  }

  const setProp = (prop, value) => {
    setTemplate({ ...template, [prop]: value });
  }

  const updateRepeatPattern = (repeatPattern) => {
    setTemplate({ ...template, repeatPattern });
  }

  const updateRepeatUntil = (datetime) => {
    setTemplate({ ...template, repeatUntil: datetime && timeToStr(datetime) });
  }

  return <>
    <ScrollView>
      {debugMode ? <Text>{templateId}</Text> : null}
      <TextInput
        value={template.title}
        onChangeText={(text) => setProp('title', text)}
        label='Title:'
        >
      </TextInput>
      <List.Item
        title='Flexible time'
        onPress={toggleFlexible}
        right={() => <>
          <Switch
            value={template.flexible}
            onValueChange={toggleFlexible}
          />
        </>}
      />
      {template.flexible
      ? <DateTimeInput
        title='Deadline'
        value={template.deadline}
        onChange={value => setProp('deadline', value)}
        default={add(new Date(), { 'days': 1 })}
      />
      : null}
      <DateTimeInput
        title={template.flexible ? 'Planned start time' : 'Start time'}
        value={template.startTime}
        onChange={value => setProp('startTime', value)}
        default={add(new Date(), { 'hours': 1 })}
      />
      {template.flexible
      ? <DurationInput
        title='Estimated duration'
        value={template.duration}
        onChange={value => setProp('duration', value)}
      />
      : <DateTimeInput
        title='End time'
        value={getEndTime(template)}
        onChange={updateEndTime}
        default={getEndTimeDefault(template)}
      />}
      <DurationInput
        title='Minimum duration'
        value={template.minDuration}
        onChange={value => setProp('minDuration', value)}
      />
      <SpoonInput
        spoons={template.spoons}
        onChange={value => setProp('spoons', value)}
      />
      <CategoriesInput
        categoryIds={template.categoryIds || []}
        onChange={value => setProp('categoryIds', value)}
      />
      <RepeatPatternInput
        value={template.repeatPattern}
        onChange={updateRepeatPattern}
      />
      <DateTimeInput
        title='Repeat until'
        value={strToTime(template.repeatUntil)}
        default={null}
        unsetTitle='Forever'
        onChange={updateRepeatUntil}
      />
      <Button
        icon='content-save'
        onPress={onSave}
        >
        Save
      </Button>
    </ScrollView>
  </>;
}

export default TemplateForm;