import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, Text, Appbar } from 'react-native-paper';
import { add } from 'date-fns';
import { useSelector, useDispatch } from 'react-redux';

import TaskList from './task_list';
import { TASK_STATUS } from '../constants/enums';
import buildSchedule from '../utils/buildSchedule';
import { formatRange, randomString } from '../utils/stringHelpers';
import { getOpenTasksInTimeRange } from '../utils/taskHelpers';
import * as appActions from '../actions/index';


const NextUp = (props) => {
  const { navigation } = props;
  const { tasks, openTasks, templates, phantomTasks } = useSelector(({task, tmp}) => {
    const { tasks, templates } = task;
    const openTasks = task[TASK_STATUS.open];
    const { phantomTasks } = tmp;
    return { tasks, openTasks, templates, phantomTasks };
  });
  const [invalidated, setInvalidated] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch();

  const now = new Date();
  const allOpenTasks = getOpenTasksInTimeRange(templates, tasks, openTasks, now, add(now, { days: 3 }));
  const newPhantomTasks = allOpenTasks.filter(t => !tasks[t.id]);
  const schedule = buildSchedule(allOpenTasks, templates);
  const taskIds = schedule && schedule.map(s => s.taskId);
  const newPhantomTaskIds = newPhantomTasks.map(pt => pt.id).join(';');
  const hasUnpreparedTasks = taskIds.filter(tid => !tasks[tid] && !phantomTasks[tid]).length > 0;

  useEffect(() => {
    setRefreshing(false);
    dispatch(appActions.createPhantomTasks(newPhantomTasks, templates));
  }, [invalidated, newPhantomTaskIds]);

  const invalidate = () => {
    dispatch(appActions.clearPhantomTasks());
    setInvalidated(randomString());
    setRefreshing(true);
  };

  return <>
    {schedule && !hasUnpreparedTasks
    ? <TaskList
      taskIds={taskIds}
      onRefresh={invalidate}
      refreshing={refreshing}
      // TODO: Display spoons?
      taskDescription={(task) => {
        const s = schedule.find(s => s.taskId === task.id);
        const range = formatRange(s.start, s.end);
        return `Do at ${range}`;
      }}
      {...props}
    />
    : <Text>Calculating...</Text>}
  </>;
}

export default NextUp;