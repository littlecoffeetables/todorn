import React, { useState } from 'react';
import {
  Button,
  List,
  Switch,
  TextInput,
  Appbar,
  Text,
} from 'react-native-paper';
import {
  ScrollView,
} from 'react-native';
import { add, differenceInMinutes, parseISO, sub } from 'date-fns';
import { useBackHandler } from '@react-native-community/hooks';
import { useDispatch, useSelector } from 'react-redux';

import DateTimeInput from '../components/datetimeinput';
import DurationInput from '../components/duration_input';
import SpoonInput from '../components/spoon_input';
import * as appActions from '../actions/index';
import { strToTime, timeDeltaToMinutes, timeToStr } from '../utils/timeHelpers';
import RepeatPatternInput from '../components/repeat_pattern_input';
import {
  ABSOLUTE_TEMPLATE_FIELDS,
  getTaskFields,
  makePhantomTask,
  makeTaskFromTemplate,
  RELATIVE_TEMPLATE_FIELDS,
  validateTask,
} from '../utils/taskHelpers';
import CategoriesInput from '../components/categories_input';


const TaskForm = (props) => {
  const { navigation } = props;
  const { taskId, templateId, days } = props.route.params;
  const { tasks, templates, debugMode } = useSelector(({task, settings}) => {
    const { tasks, templates } = task;
    const { debugMode } = settings;
    return { tasks, templates, debugMode };
  });
  const dispatch = useDispatch();

  const originalTask = tasks[taskId];
  const originalTemplate = originalTask
    ? (originalTask.templateId && templates[originalTask.templateId])
    : templates[templateId];
  const [template, setTemplate] = useState(
    originalTemplate ?? { repeatPattern: null, repeatUntil: null }
  );
  const [task, setTask] = useState(
    getTaskFields(originalTask ?? makePhantomTask(originalTemplate, days), originalTemplate)
  );
  const uneditedTask = { ...task };
  const [editTemplate, setEditTemplate] = useState(false);

  const onSave = () => {
    const changes = { ...task };
    delete changes.id;
    if (!editTemplate || !task.startTime) {
      if (originalTask) {
        dispatch(appActions.updateTask(task, changes));
      }
      // Phantom task
      else if (originalTemplate) {
        dispatch(appActions.createTaskFromTemplate(originalTemplate.id, days, changes));
      }
    }
    else {
      if (originalTemplate) {
        const templateChanges = {};
        ABSOLUTE_TEMPLATE_FIELDS.forEach(key => {
          templateChanges[key] = task[key]
        });
        RELATIVE_TEMPLATE_FIELDS.forEach(key => {
          templateChanges[key] = task[key] && timeToStr(sub(strToTime(task[key]), { days: task.days }));
        });
        ['repeatPattern', 'repeatUntil']
          .filter(key => template[key] !== originalTemplate[key])
          .forEach(key => templateChanges[key] = template[key]);
        dispatch(appActions.updateTemplate(originalTemplate.id, task, templateChanges, true));
      }
      else {
        dispatch(appActions.createNewTemplate(task, template.repeatPattern, template.repeatUntil));
      }
    }
    // props.afterSave(task.id, changes, templateChanges, true);
    navigation.goBack();
  }

  const onCancel = () => {
    navigation.goBack();
  }

  // useBackHandler(() => {
  //   onCancel();
  //   return true;
  // })

  const updateEndTime = (datetime) => {
    if (!task.startTime) return;
    const duration = timeDeltaToMinutes(strToTime(datetime) - strToTime(task.startTime));
    if (duration >= 0) {
      setTask({ ...task, duration });
    }
  }

  const toggleFlexible = () => {
    setTask({ ...task, flexible: !task.flexible });
  }

  const getEndTime = () => {
    return task.startTime && task.duration ? add( strToTime(task.startTime), { minutes: task.duration } ) : undefined;
  }

  const getEndTimeDefault = () => {
    return add( strToTime(task.startTime) || new Date, { minutes: task.duration || 120 } );
  }

  const setProp = (prop, value) => {
    setTask({ ...task, [prop]: value });
  }

  const toggleEditTemplate = () => {
    setEditTemplate(!editTemplate);
  }

  const updateRepeatPattern = (repeatPattern) => {
    setTemplate({ ...template, repeatPattern });
  }

  const updateRepeatUntil = (datetime) => {
    setTemplate({ ...template, repeatUntil: datetime && timeToStr(datetime) });
  }

  return <>
    <ScrollView>
      {debugMode ? <Text>{task.templateId}</Text> : null}
      <TextInput
        value={task.title}
        onChangeText={(text) => setProp('title', text)}
        label='Title:'
        >
      </TextInput>
      <List.Item
        title='Flexible time'
        onPress={toggleFlexible}
        right={() => <>
          <Switch
            value={task.flexible}
            onValueChange={toggleFlexible}
          />
        </>}
      />
      {task.flexible
      ? <DateTimeInput
        title='Deadline'
        value={task.deadline}
        onChange={value => setProp('deadline', value)}
        default={add(new Date(), { 'days': 1 })}
      />
      : null}
      <DateTimeInput
        title={task.flexible ? 'Planned start time' : 'Start time'}
        value={task.startTime}
        onChange={value => setProp('startTime', value)}
        default={add(new Date(), { 'hours': 1 })}
      />
      {task.flexible
      ? <DurationInput
        title='Estimated duration'
        value={task.duration}
        onChange={value => setProp('duration', value)}
      />
      : <DateTimeInput
        title='End time'
        value={getEndTime(task)}
        onChange={updateEndTime}
        default={getEndTimeDefault(task)}
      />}
      <DurationInput
        title='Minimum duration'
        value={task.minDuration}
        onChange={value => setProp('minDuration', value)}
      />
      <SpoonInput
        spoons={task.spoons}
        onChange={value => setProp('spoons', value)}
      />
      <CategoriesInput
        categoryIds={task.categoryIds || []}
        onChange={value => setProp('categoryIds', value)}
      />
      <List.Item
        title={originalTemplate ? 'Edit repetition template' : 'Create repetition template'}
        onPress={toggleEditTemplate}
        description={task.startTime ? null : 'Start time is required for repetition'}
        right={() => <>
          <Switch
            value={editTemplate && !!task.startTime}
            onValueChange={toggleEditTemplate}
            disabled={!task.startTime}
          />
        </>}
      />
      {editTemplate && template && task.startTime ? <>
        <RepeatPatternInput
          value={template.repeatPattern}
          onChange={updateRepeatPattern}
        />
        <DateTimeInput
          title='Repeat until'
          value={strToTime(template.repeatUntil)}
          default={null}
          unsetTitle='Forever'
          onChange={updateRepeatUntil}
        />
      </> : null}
      <Button
        icon='content-save'
        onPress={onSave}
        >
        Save
      </Button>
    </ScrollView>
  </>;
}

export default TaskForm;