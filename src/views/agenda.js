import React, { useEffect, useState } from 'react';
import { add, format, getDate, getDaysInMonth, set, startOfDay } from 'date-fns';
import { View, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Agenda } from 'react-native-calendars';
import { Text } from 'react-native-paper';
import { useSelector } from 'react-redux';

import { TASK_STATUS } from '../constants/enums';
import { getOpenTasksInTimeRange, getTaskFields } from '../utils/taskHelpers';
import { COLORS } from '../constants/colors';
import { formatDatetime, formatRange } from '../utils/stringHelpers';
import { strToTime } from '../utils/timeHelpers';
import { DAILY_CUTOFF } from '../constants/misc';


const normalDot = { key: 'n', color: '#0af', selectedDotColor: '#fff' };
const priorityDot = { key: 'p', color: 'red' };
const deadlineDot = { key: 'd', color: 'black' };


const AgendaView = (props) => {
  const { tasks, templates, openTasks, categories } = useSelector(({task}) => {
    const { tasks, templates, categories } = task;
    const openTasks = task[TASK_STATUS.open];
    return { tasks, templates, openTasks, categories };
  });
  const [items, setItems] = useState({});
  const [refreshing, setRefreshing] = useState(false);
  const [selectedDay, setSelectedDay] = useState(null);


  const loadItems = (day) => {
    if (!day || !day.timestamp) {
      day = {timestamp: (new Date()).valueOf()};
    }
    // daily cutoff at 3:00
    const baseDate = add(startOfDay(new Date(day.timestamp)), DAILY_CUTOFF);
    const dayOfMonth = getDate(baseDate);
    const maxDayOfMonth = getDaysInMonth(baseDate);
    const newItems = {};
    for (i = 1 - dayOfMonth; i <= maxDayOfMonth - dayOfMonth; ++i) {
      const date = add(baseDate, {days: i});
      const dateString = format(date, 'yyyy-MM-dd');
      if (items[dateString]) return;
      const dayTasks = getOpenTasksInTimeRange(templates, tasks, openTasks, date, add(date, {days: 1}), true)
        .map(t => {
          const task = getTaskFields(t, templates[t.templateId]);
          return { ...task, comparison: strToTime(task.startTime) ?? strToTime(task.deadline) ?? 0 }
        });
      dayTasks.sort((a, b) => {
        return a.comparison - b.comparison;
      });
      newItems[dateString] = dayTasks.map(task => {
        const range = task.startTime && formatRange(
          strToTime(task.startTime),
          add(strToTime(task.startTime), { minutes: task.duration ?? task.minDuration ?? 60 })
        );
        const deadline = formatDatetime(task.deadline);
        return {
          name: task.title,
          height: 80,
          subtitle: range ? `At ${range}` : `Deadline at ${deadline}`,
          category: categories[task.categoryIds?.[0]],
        };
      });
    }
    setItems({ ...items, ...newItems });
    if (refreshing) {
      setRefreshing(false);
    }
  }  
  
  const refresh = () => {
    setItems({});
    setRefreshing(true);
  }
  
  useEffect(() => {
    if (refreshing) {
      loadItems({ timestamp: selectedDay });
    }
  }, [refreshing]);

  useEffect(() => {
    loadItems({ timestamp: selectedDay });
  }, [selectedDay]);

  const selectDay = (day) => {
    setSelectedDay(day.timestamp);
  }

  const renderItem = (item) => {
    return (
      <View
        style={[
          styles.item,
          {height: item.height},
          {backgroundColor: item.category?.color ? `#${item.category.color}` : COLORS.background}
        ]}
        // onPress={() => Alert.alert(reservation.name)}
      >
        <Text style={styles.itemText}>{item.name}</Text>
        <Text style={styles.itemSubText}>{item.subtitle}</Text>
      </View>    
    );
  }  

  return <>
      <Agenda
        // selected={'2022-10-31'}
        firstDay={1}
        items={items}
        loadItemsForMonth={loadItems}
        renderItem={renderItem}
        onDayPress={selectDay}
        markingType={'multi-dot'}
        // markedDates={{
        //   '2022-10-30': {dots: [normalDot, deadlineDot]},
        //   '2022-10-31': {dots: [normalDot], selected: true},
        //   '2022-11-01': {dots: [normalDot, priorityDot]},
        // }}
        theme={{
          backgroundColor: COLORS.background,
          calendarBackground: COLORS.background,
          monthTextColor: COLORS.text,
          dayTextColor: COLORS.text,
          reservationsBackgroundColor: COLORS.alt,
          agendaDayNumColor: COLORS.text,
          agendaDayTextColor: COLORS.dimText,
          'stylesheet.calendar.header': {
            dayTextAtIndex6: {
              color: '#b44',
            }
          },
        }}
        onRefresh={refresh}
        refreshing={refreshing}
      />
  </>;
}


const styles = StyleSheet.create({
  item: {
    backgroundColor: COLORS.primary,
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30
  },
  itemText: {
    fontSize: 16,
    color: COLORS.text,
  },
  itemSubText: {
    fontSize: 16,
    color: COLORS.text,
  },
});

export default AgendaView;