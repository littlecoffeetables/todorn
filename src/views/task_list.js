import React, { useState } from 'react';
import { RefreshControl, ScrollView, StyleSheet, View } from 'react-native';
import { useSelector } from 'react-redux';

import { TASK_STATUS } from '../constants/enums';
import TaskListItem from '../components/task_list_item';
import { Snackbar } from 'react-native-paper';
import { COLORS, theme } from '../constants/colors';
import NewTaskButton from '../components/new_task_button';


const TaskList = (props) => {
  const { showNewTaskButton } = props;
  const { openTasks } = useSelector(({task}) => {
    const openTasks = task[TASK_STATUS.open];
    return { openTasks };
  });
  const [shownMessage, setShownMessage] = useState(null);

  const taskList = props.taskIds ?? openTasks;

  return <>
    <ScrollView
      refreshControl={
        props.onRefresh
        ? <RefreshControl
          refreshing={props.refreshing}
          onRefresh={props.onRefresh}
        />
        : null}
      >
      {taskList.map(taskId => {
        return <TaskListItem key={taskId} taskId={taskId} setShownMessage={setShownMessage} {...props} />
      })}
    </ScrollView>
    <View style={styles.container}>
      {showNewTaskButton ? <NewTaskButton /> : null}
      <Snackbar
        visible={shownMessage !== null}
        onDismiss={() => setShownMessage(null)}
        duration={5000}
        style={{...styles.snackbar, marginBottom: showNewTaskButton ? 80 : null}}
        action={shownMessage?.action}
        theme={snackbarTheme}
        >
        {shownMessage?.label}
      </Snackbar>
    </View>
  </>;
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  snackbar: {
    color: COLORS.text,
  }
});

const snackbarTheme = {
  ...theme,
  colors: {
    ...theme.colors,
    surface: COLORS.text,
  }
}



export default TaskList;