import React from 'react';
import { View } from 'react-native';
import NewTaskButton from '../components/new_task_button';

import TaskList from './task_list';


const MainList = (props) => {
  return <>
    <TaskList
      showNewTaskButton={true}
      {...props}
    />
  </>;
}

export default MainList;