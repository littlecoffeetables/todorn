import React, { useCallback, useEffect, useState } from 'react';
import { RefreshControl, ScrollView } from 'react-native';
import {
  Text,
  List,
  IconButton,
  Provider,
} from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';

import { theme } from '../constants/colors';
import { TASK_STATUS } from '../constants/enums';
import { formatDatetime } from '../utils/stringHelpers';
import { timeToStr } from '../utils/timeHelpers';
import * as appActions from '../actions/index';


const TemplateList = (props) => {
  const { navigation } = props;
  const { templates, debugMode } = useSelector(({task, settings}) => {
    const { templates } = task;
    const { debugMode } = settings;
    return { templates, debugMode };
  });

  const getDescription = (template) => {
  }

  const templateClicked = (template) => {
    navigation.navigate('Edit Template', { templateId: template.id });
  }

  const templateList = Object.keys(templates);

  return (
    <ScrollView>
      {templateList.map(templateId => {
        const template = templates[templateId];
        return <List.Item
          key={templateId}
          title={`${template.title ?? 'Untitled'}${debugMode ? ` [${template.id}]` : ''}`}
          description={getDescription(template)}
          theme={{colors: {
            text: 'white',
          }}}
          onPress={() => templateClicked(template)}
          />;
      })}
    </ScrollView>
  );
}

export default TemplateList;