import React from 'react';
import { useSelector } from 'react-redux';

import TaskList from './task_list';
import { formatDatetime } from '../utils/stringHelpers';
import { TASK_STATUS } from '../constants/enums';

const Archive = (props) => {
  const { tasks } = useSelector(({task}) => {
    const { tasks } = task;
    return { tasks };
  });

  return <>
    <TaskList
      taskIds={Object.values(tasks).filter(t => t.status !== TASK_STATUS.open).map(t => t.id)}
      taskDescription={(task) => {
        if (task.status === 'done') {
          return `Done at ${formatDatetime(task.doneAt)}`;
        } else if (task.status === 'closed') {
          return 'Closed';
        }
      }}
      {...props}
    />
  </>;
}

export default Archive;