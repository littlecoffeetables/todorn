import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, Text } from 'react-native-paper';
import add from 'date-fns/add';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { cancelNotification, listScheduledNotifications, scheduleNotification } from '../utils/notifications';
import { navTheme } from '../constants/colors';
import MainListTab from '../screens/main_list_tab';
import NextUpTab from '../screens/next_up_tab';
import ArchiveTab from '../screens/archive_tab';
import TemplateListTab from '../screens/template_list_tab';
import CalendarTab from '../screens/calendar_tab';
import SettingsScreen from '../screens/settings_screen';
import Navbar from '../components/navbar';
import CategoryListScreen from '../screens/category_list_screen';

const Tabs = createMaterialBottomTabNavigator();
const Stack = createNativeStackNavigator();


const Main = (props) => {
  return (
    <NavigationContainer theme={navTheme}>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen
          name='MainTabs'
          component={MainTabs}
          />
        <Stack.Screen
          name='Settings'
          component={SettingsScreen}
          options={{header: Navbar, headerShown: true, hideSettings: true}}
          />
        <Stack.Screen
          name='Manage Categories'
          component={CategoryListScreen}
          options={{header: Navbar, headerShown: true, hideSettings: true}}
          />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const MainTabs = (props) => {
  return (
    <Tabs.Navigator>
      <Tabs.Screen
        name='Task List'
        component={MainListTab}
        options={{
          tabBarIcon: ({color}) => <Icon name='format-list-bulleted-square' size={24} color={color} />,
        }}
      />
      <Tabs.Screen
        name='Next Up'
        component={NextUpTab}
        options={{
          tabBarIcon: ({color}) => <Icon name='inbox' size={24} color={color} />,
        }}
      />
      <Tabs.Screen
        name='Calendar'
        component={CalendarTab}
        options={{
          tabBarIcon: ({color}) => <Icon name='calendar-month' size={24} color={color} />,
        }}
      />
      <Tabs.Screen
        name='Template List'
        component={TemplateListTab}
        options={{
          tabBarIcon: ({color}) => <Icon name='file-refresh-outline' size={24} color={color} />,
        }}
      />
      <Tabs.Screen
        name='Archive'
        component={ArchiveTab}
        options={{
          tabBarIcon: ({color}) => <Icon name='archive' size={24} color={color} />,
        }}
      />
        {/* <Button onPress={() => scheduleNotification(add(new Date(), {seconds: 10}), 'Hi there', 'Wheee', 'testid')}>Schedule test notification</Button> */}
        {/* <Button onPress={() => listScheduledNotifications(console.log)}>Debug notifications</Button> */}
    </Tabs.Navigator>
  );
}

export default Main;