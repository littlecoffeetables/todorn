import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider as PaperProvider, Text } from 'react-native-paper';

import Main from './main';
import { store, persistor } from '../storage/store';
import { theme } from '../constants/colors';

// if(__DEV__) {
//   import('./config/ReactotronConfig').then(() => console.log('Reactotron Configured'))
// }

// persistor.purge();

const App = () => {
  return <Provider store={store}>
    <PersistGate persistor={persistor}>
      <PaperProvider theme={theme}>
        <Main />
      </PaperProvider>
    </PersistGate>
  </Provider>
}

export default App;