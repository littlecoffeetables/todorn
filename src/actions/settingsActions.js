import * as types from './actionTypes';

export const setSetting = (setting, value) => {
  return {
    type: types.SET_SETTING,
    setting,
    value,
  };
}
