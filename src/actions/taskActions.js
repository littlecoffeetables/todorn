import * as types from './actionTypes';

export const createNewTask = () => {
  return {
    type: types.CREATE_NEW_TASK,
  };
}

export const updateTask = (task, changes) => {
  return {
    type: types.UPDATE_TASK,
    task,
    changes,
  };
}

export const deleteTask = (task) => {
  return {
    type: types.DELETE_TASK,
    task,
  };
}

export const createTaskFromTemplate = (templateId, days, changes) => {
  return {
    type: types.CREATE_TASK_FROM_TEMPLATE,
    templateId,
    days,
    changes,
  }
}



export const createPhantomTasks = (tasks, templates) => {
  return {
    type: types.CREATE_PHANTOM_TASKS,
    tasks,
    templates,
  }
}


export const clearPhantomTasks = () => {
  return {
    type: types.CLEAR_PHANTOM_TASKS,
  }
}



export const createNewTemplate = (task, repeatPattern, repeatUntil) => {
  return {
    type: types.CREATE_NEW_TEMPLATE,
    task,
    repeatPattern,
    repeatUntil,
  }
}

export const updateTemplate = (templateId, task, changes, stripTask=false) => {
  return {
    type: types.UPDATE_TEMPLATE,
    templateId,
    task,
    changes,
    stripTask,
  }
}

// export const deleteTemplate = (templateId) => {
//   return {
//     type: types.DELETE_TEMPLATE,
//     templateId,
//   }
// }


export const createOrUpdateCategory = (changes) => {
  return {
    type: types.CREATE_OR_UPDATE_CATEGORY,
    changes,
  }
}

