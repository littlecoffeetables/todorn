import React, { useEffect, useState } from 'react';
import { ScrollView, Dimensions } from 'react-native';
import {
  Button,
  Dialog,
  List,
  Portal,
  RadioButton,
  Text,
} from 'react-native-paper';

import NumberInput from './number_input';


const DurationInput = (props) => {
  const [mode, setMode] = useState(null);
  const [tmpValue, setTmpValue] = useState(props.value ?? 60);

  const value = props.value;

  const updateValue = (newVal) => {
    setMode(null);
    props.onChange(newVal);
  }

  const hideDialog = () => setMode(null);

  const toggleMode = () => setMode(mode === 'list' ? 'exact' : 'list');

  useEffect(() => {
    setTmpValue(props.value ?? 60);
  }, [props.value]);

  return (
    <>
      <List.Item
        title={props.title}
        description={value ? formatDuration(value) : 'Not set'}
        right={() => <>
          <Button onPress={() => setMode('list')} icon='clock-outline'>
            {value ? formatDuration(value) : 'Not set'}
          </Button>
        </>}
      />
      <Portal>
        <Dialog
          visible={mode}
          onDismiss={hideDialog}
          style={{
            backgroundColor: '#666',
            maxHeight: 0.9 * Dimensions.get('window').height,
          }}
        >
          <Dialog.Title>Choose duration</Dialog.Title>
          {mode === 'list'
          ? <Dialog.ScrollArea>
            <ScrollView>
              <RadioButton.Group onValueChange={updateValue} value={value} >
                <RadioButton.Item label='Unset' value={undefined} />
                <RadioButton.Item label='5 minutes' value={5} />
                <RadioButton.Item label='10 minutes' value={10} />
                <RadioButton.Item label='20 minutes' value={20} />
                <RadioButton.Item label='30 minutes' value={30} />
                <RadioButton.Item label='45 minutes' value={45} />
                <RadioButton.Item label='1 hour' value={60} />
                <RadioButton.Item label='1 hour 30 minutes' value={90} />
                <RadioButton.Item label='2 hours' value={120} />
                {/* <RadioButton.Item label='2 hours 30 minutes' value={150} /> */}
                <RadioButton.Item label='3 hours' value={180} />
                <RadioButton.Item label='5 hours' value={300} />
                <RadioButton.Item label='8 hours' value={8 * 60} />
                <RadioButton.Item label='12 hours' value={12 * 60} />
                <RadioButton.Item label='20 hours' value={20 * 60} />
                <RadioButton.Item label='30 hours' value={30 * 60} />
                <RadioButton.Item label='50 hours' value={50 * 60} />
                <RadioButton.Item label='80 hours' value={80 * 60} />
                <RadioButton.Item label='120 hours' value={120 * 60} />
              </RadioButton.Group>

            </ScrollView>
            </Dialog.ScrollArea>
          : <Dialog.Content>
            <Text>Minutes:</Text>
            <NumberInput
              onChange={setTmpValue}
              minValue={0}
              initialValue={tmpValue}
              helperText={formatDuration(tmpValue)}
              maxLength={5}
              />
          </Dialog.Content>}
          <Dialog.Actions style={{justifyContent: 'space-between'}}>
            <Button onPress={hideDialog}>Cancel</Button>
            <Button onPress={toggleMode}>{mode === 'list' ? 'Custom' : 'Preset'}</Button>
            <Button onPress={() => updateValue(tmpValue)}>Done</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </>
  );
}

const zeroPad = (value) => {
  const str = '' + value;
  return str.length < 2 ? '0'+str : str;
}

const formatDuration = (value) => {
  const days = parseInt(value / 60 / 24);
  return `${days > 0 ? `${days}d ` : ''}${zeroPad(parseInt(value / 60 % 24))}:${zeroPad(parseInt(value % 60))}`;
}

export default DurationInput;