import React, { useState } from 'react';
import {
  List,
  Portal,
  Dialog,
  Text,
  TouchableRipple,
} from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { TASK_STATUS } from '../constants/enums';
import { formatDatetime } from '../utils/stringHelpers';
import { strToTime, timeToStr } from '../utils/timeHelpers';
import * as appActions from '../actions/index';
import { getTaskFields, isOpen } from '../utils/taskHelpers';
import { COLORS } from '../constants/colors';
import DateTimePicker from '@react-native-community/datetimepicker';


const TaskListItem = (props) => {
  const { taskId, navigation, setShownMessage } = props;
  const { tasks, phantomTasks, templates, debugMode } = useSelector(({task, tmp, settings}) => {
    const { tasks, templates } = task;
    const { phantomTasks } = tmp;
    const { debugMode } = settings;
    return { tasks, phantomTasks, templates, debugMode };
  });
  const rawTask = tasks[taskId] ?? phantomTasks[taskId];
  const template = templates[rawTask.templateId];
  const task = getTaskFields(rawTask, template);
  const [showDialog, setShowDialog] = useState(false);
  const [chosenTime, setChosenTime] = useState(null);
  const [timeInputPhase, setTimeInputPhase] = useState(null);
  const [followupAction, setFollowupAction] = useState(null);
  
  const dispatch = useDispatch();

  const getDescription = (task) => {
    if (props.taskDescription) {
      return props.taskDescription(task);
    } else if (task.deadline) {
      return formatDatetime(task.deadline);
    } else if (task.startTime) {
      return formatDatetime(task.startTime);
    }
    return null;
  }

  const taskClicked = () => {
    setShowDialog(true);
  }
  
  const editTask = () => {
    dismissDialog();
    navigation.navigate('Edit Task', { taskId: task.id, days: task.days, templateId: task.templateId });
  }

  const deleteTask = () => {
    dispatch(appActions.deleteTask(task));
    setShownMessage({label: `Task ${task.title} deleted.`});
  }

  const doneTask = (evt, datetime) => {
    dispatch(appActions.updateTask(task, { status: TASK_STATUS.done, doneAt: timeToStr(datetime || new Date()) }));
    setShownMessage({
      label: `Task ${task.title} done!`,
      action: {
        label: 'Undo',
        onPress: () => reopenTask(false),
      }
    });
    dismissDialog();
  }

  const reopenTask = (showMessage = true) => {
    dispatch(appActions.updateTask(task, { status: TASK_STATUS.open }));
    if (showMessage) {
      setShownMessage({
        label: `Task ${task.title} unarchived!`,
        action: {
          label: 'Undo',
          onPress: () => dispatch(appActions.updateTask(task, { status: task.status })),
        }
      });
    }
  }

  const archiveTask = () => {
    dispatch(appActions.updateTask(task, { status: TASK_STATUS.closed }));
    setShownMessage({
      label: `Task ${task.title} closed!`,
      action: {
        label: 'Undo',
        onPress: () => reopenTask(false),
      }
    });
  }

  const followupActions = {
    'doneTask': doneTask,
  };

  const doneTaskAt = () => {
    setChosenTime(strToTime(task.doneAt) || null);
    setTimeInputPhase('date');
    setFollowupAction('doneTask');
  }

  const updateTime = (evt, value) => {
    if (evt.type === 'dismissed') {
      setTimeInputPhase(null);
      setFollowupAction(null);
    } else {
      if (timeInputPhase === 'date') {
        setTimeInputPhase('time');
      } else if (followupAction) {
        setTimeInputPhase(null);
        followupActions[followupAction](null, value);
      }
      setChosenTime(value);
    }
  }

  const dismissDialog = () => setShowDialog(false);
  const dismissMessage = () => setShownMessage(null);

  const allActions = {
    // TODO
    start: { label: 'Start', onPress: dismissDialog, icon: 'clock-fast' },
    done: { label: 'Done', onPress: doneTask, icon: 'check-underline' },
    doneAt: { label: 'Done At...', onPress: doneTaskAt, icon: 'clock-check-outline' },
    edit: { label: 'Edit', onPress: editTask, icon: 'pencil' },
    // TODO
    delay: { label: 'Delay', onPress: dismissDialog, icon: 'clock-plus' },
    archive: { label: 'Archive', onPress: archiveTask, icon: 'archive' },
    reopen: { label: 'Reopen', onPress: reopenTask, icon: 'undo-variant' },
    delete: { label: 'Delete', onPress: deleteTask, icon: 'trash-can-outline', color: COLORS.delete },
  };

  const actions = [];
  if (isOpen(task)) {
    ['edit', 'start', 'done', 'doneAt', 'delay', 'archive'].forEach(a => actions.push(allActions[a]));
  }
  else {
    ['edit', 'done', 'doneAt', 'reopen', 'delete']
      .filter(a => (a === 'done' || a === 'delete') ? task.status !== TASK_STATUS.done : true)
      .forEach(a => actions.push(allActions[a]));
  }

  return <>
    <Portal>
      <Dialog visible={showDialog} onDismiss={dismissDialog}>
        <Dialog.Title>{task.title}</Dialog.Title>
        <Dialog.Content style={{}}>
          {actions.map(a =>
            <TouchableRipple key={a.label} onPress={a.onPress}>
              <View style={styles.dialogItemContainer}>
                <Text style={{...styles.dialogItemText, color: (a.color ?? null)}}>
                  {a.label}
                </Text>
                {a.icon
                ? <Icon
                  style={{marginLeft: 10}}
                  size={24}
                  name={a.icon}
                  color={a.color ?? null}
                  />
                : null}
              </View>
            </TouchableRipple>
          )}
        </Dialog.Content>
      </Dialog>
    </Portal>
    <List.Item
      title={`${task.title ?? 'Untitled'}${debugMode ? ` [${task.id}]` : ''}`}
      description={getDescription(task)}
      theme={{colors: {
        text: 'white',
      }}}
      onPress={() => taskClicked(task)}
    />
    {timeInputPhase
    ? <DateTimePicker
      value={chosenTime ?? new Date()}
      onChange={updateTime}
      mode={timeInputPhase}
      />
    : null}
  </>;
}


const styles = StyleSheet.create({
  dialogItemContainer: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  dialogItemText: {
    flexShrink: 1,
    flexGrow: 1,
    fontSize: 18,
  }
})


export default TaskListItem;