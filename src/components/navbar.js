import React from "react";
import { StyleSheet } from "react-native";
import { Appbar } from "react-native-paper";
import { COLORS, theme } from "../constants/colors";



const Navbar = (props) => {
  const { back, options, navigation, route } = props;

  const openSettings = () => navigation.push('Settings');

  return (
    <Appbar.Header style={styles.header} theme={headerTheme}>
      {back ? <Appbar.BackAction onPress={navigation.goBack} /> : null}
      <Appbar.Content title={options.title ?? route.name} />
      {(options.headerActions || []).map(a => {
        return <Appbar.Action key={a.icon} icon={a.icon} onPress={a.onPress} />
      })}
      {!options.hideSettings
      ? <Appbar.Action icon={'dots-vertical'} onPress={openSettings} />
      : null}
    </Appbar.Header>
  );
};

const styles = StyleSheet.create({
  header: {
    borderBottomColor: '#111',
    borderBottomWidth: 2,
  },
})

const headerTheme = {
  ...theme,
  colors: {
    ...theme.colors,
    primary: COLORS.alt,
  }
}

export default Navbar;