import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Button,
  Chip,
  Dialog,
  IconButton,
  Portal,
  Text,
  TouchableRipple,
} from 'react-native-paper';
import { useSelector } from 'react-redux';


const CategoriesInput = (props) => {
  const { allCategories } = useSelector(({task}) => {
    const { categories } = task;
    return { allCategories: categories };
  })
  const [open, setOpen] = useState(false);

  const addCategory = (category) => {
    props.onChange([ ...props.categoryIds, category.id ]);
    setOpen(false);
  }

  const removeCategory = (category) => {
    props.onChange(props.categoryIds.filter(cid => cid !== category.id));
    setOpen(false);
  }

  const hideDialog = () => setOpen(false);
  const showDialog = () => setOpen(true);

  return <>
    <TouchableRipple onPress={props.categoryIds.length === 0 ? showDialog : null}>
      <View style={styles.container}>
        <View style={styles.chipContainer}>
          {props.categoryIds.length === 0
          ? <Text style={styles.blankText}>Add categories...</Text>
          : null}
          {props.categoryIds.map(cid => {
            const c = allCategories[cid];
            return <Chip
              key={c.id}
              onClose={() => removeCategory(c)}
              style={{...styles.chip, backgroundColor: `#${c.color}`}}
            >
              {c.name}
            </Chip>
          })}
        </View>
        <IconButton
          icon='plus'
          onPress={showDialog}
          style={styles.addButton}
        />
      </View>
    </TouchableRipple>
    <Portal>
      <Dialog
        visible={open}
        onDismiss={hideDialog}
        style={{ backgroundColor: '#666' }}
      >
        <Dialog.Title>Add category</Dialog.Title>
        <Dialog.ScrollArea>
          {Object.values(allCategories).map(c => {
            if (c.deleted || props.categoryIds.indexOf(c.id) > -1) return;
            return <Chip
              key={c.id}
              onPress={() => addCategory(c)}
              style={{backgroundColor: `#${c.color}`}}
            >
              {c.name}
            </Chip>
          })}
        </Dialog.ScrollArea>
        <Dialog.Actions>
          <Button onPress={hideDialog}>Cancel</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  </>
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  chipContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    margin: 10,
    marginRight: 0,
  },
  chip: {
    margin: 3,
  },
  addButton: {
    alignSelf: 'center',
  },
  blankText: {
    margin: 5,
  }
})

export default CategoriesInput;