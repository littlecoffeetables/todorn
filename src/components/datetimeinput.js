import React, { useState } from 'react';
import {
  Button,
  IconButton,
  List,
} from 'react-native-paper';
import DateTimePicker from '@react-native-community/datetimepicker';
import format from 'date-fns/format';

import { timeToStr, strToTime } from '../utils/timeHelpers';
import { COLORS } from '../constants/colors';


const DateTimeInput = (props) => {
  const propValue = strToTime(props.value);
  const [state, setState] = useState({
    value: propValue,
    useValue: false,
    mode: null,
  });

  const updateValue = (evt, newVal) => {
    if (evt.type === 'dismissed') {
      setState({
        mode: null,
        value: propValue,
        useValue: false,
      });
      return;
    }
    newVal.setSeconds(0);
    newVal.setMilliseconds(0);
    if (state.mode === 'both') {
      setState({
        mode: 'time',
        value: newVal,
        useValue: true,
      });
    }
    else {
      setState({
        mode: null,
        value: newVal,
        useValue: false,
      });
      props.onChange(timeToStr(newVal));
    }
  }

  const setMode = (mode) => {
    setState({...state, mode: mode});
  }

  const unsetValue = () => {
    const newVal = props.default || null;
    props.onChange(null);
    setState({...state, value: newVal});
  }

  return (
    <>
      <List.Item
        title={props.title}
        description={propValue ? format(propValue, 'd.M.y HH:mm') : props.unsetTitle ?? 'Not set'}
        onPress={() => setMode('both')}
        right={() => <>
          <Button onPress={() => setMode('date')} icon='calendar-month'>
            {propValue ? format(propValue, 'd.M.y') : 'Set date'}
          </Button>
          <Button onPress={() => setMode('time')} icon='clock'>
            {propValue ? format(propValue, 'HH:mm') : 'Set time'}
          </Button>
          <IconButton onPress={unsetValue} icon='eraser' color={COLORS.primary} size={22} />
        </>}
      />
      {state.mode
      ? <DateTimePicker
        value={(state.useValue ? state.value : propValue) || props.default}
        onChange={updateValue}
        mode={state.mode === 'both' ? 'date' : state.mode}
        />
      : null}
    </>
  );
}

export default DateTimeInput;