import React, { useState } from 'react';
import { ScrollView, View } from 'react-native';
import {
  Button,
  Dialog,
  List,
  Portal,
  Text,
  TextInput,
  RadioButton,
  Chip,
} from 'react-native-paper';

import { DAYS_PER_MONTH, WEEKDAYS } from '../constants/calendar';
import { PATTERN_MODE } from '../constants/enums';
import NumberInput from './number_input';
import { validateRepeatPattern, trimRepeatPattern, getRepeatPatternDescription } from '../utils/taskHelpers';


const RepeatPatternInput = (props) => {
  const [phase, setPhase] = useState(0);
  const [pattern, setPattern] = useState(props.value ?? {});

  const updatePattern = () => {
    const trimmed = trimRepeatPattern(pattern);
    props.onChange(trimmed);
    setPhase(0);
    setPattern(trimmed);
  }

  const prevPhase = () => setPhase(phase - 1);

  const dismissDialog = () => {
    setPhase(0);
    setPattern(props.value ?? {});
  };

  const setMode = (mode) => {
    setPattern({ ...pattern, mode });
    if (mode) {
      setPhase(phase + 1);
    } else {
      setPhase(0);
    }
  }


  const setN = (value) => {
    const key = `N_${pattern.mode}`;
    setPattern({ ...pattern, [key]: value });
  }

  const toggleWeekly = (i) => {
    const weekly = [ ...(pattern.weekly ?? []) ];
    weekly[i] = !weekly[i];
    setPattern({ ...pattern, weekly  });
  }

  const getMonthly = (dayNumber) => {
    const key = dayNumber < 0 ? 'reverseMonthly' : 'monthly';
    const i = (dayNumber < 0 ? -dayNumber : dayNumber) - 1;
    return (pattern[key] ?? [])[i];
  }

  const toggleMonthly = (dayNumber) => {
    const key = dayNumber < 0 ? 'reverseMonthly' : 'monthly';
    const i = (dayNumber < 0 ? -dayNumber : dayNumber) - 1;
    const monthly = [ ...(pattern[key] ?? []) ];
    monthly[i] = !monthly[i];
    setPattern({ ...pattern, [key]: monthly  });
  }

  return <>
    <List.Item
      title={`Repeat pattern`}
      onPress={() => setPhase(1)}
      // right={() => <Text>Not set</Text>}
      description={getRepeatPatternDescription(pattern)}
    />
    <Portal>
      <Dialog
        visible={phase > 0}
        onDismiss={dismissDialog}
        style={{ backgroundColor: '#666' }}
      >
        {phase === 1 ? <>
          <Dialog.Title>{'Choose mode'}</Dialog.Title>
          <Dialog.ScrollArea>
            <ScrollView>
              <RadioButton.Group onValueChange={setMode} value={pattern.mode} >
                <RadioButton.Item label='Unset' value={null} />
                <RadioButton.Item label='Every N days' value={PATTERN_MODE.days} />
                <RadioButton.Item label='Weekly schedule' value={PATTERN_MODE.weekly} />
                <RadioButton.Item label='Every N months' value={PATTERN_MODE.months} />
                <RadioButton.Item label='Monthly schedule' value={PATTERN_MODE.monthly} />
              </RadioButton.Group>
            </ScrollView>
          </Dialog.ScrollArea>
          <Dialog.Actions>
            <Button onPress={prevPhase}>Cancel</Button>
          </Dialog.Actions>
        </> : <>
          {pattern.mode === PATTERN_MODE.days || pattern.mode === PATTERN_MODE.months
          ? <>
            <Dialog.Title>{'Set frequency'}</Dialog.Title>
            <Dialog.Content>
              <Text>Repeat every N {pattern.mode}:</Text>
              <NumberInput
                onChange={setN}
                minValue={1}
                initialValue={pattern[`N_${pattern.mode}`] ?? 1}
                />
            </Dialog.Content>
          </>
          : null}
          {pattern.mode === PATTERN_MODE.weekly
          ? <>
            <Dialog.Title>{'Set schedule'}</Dialog.Title>
            <Dialog.Content>
              <Text>Pattern length in weeks (1-10):</Text>
              <NumberInput
                onChange={setN}
                minValue={1}
                maxValue={10}
                maxLength={2}
                initialValue={pattern.N_weekly ?? 1}
              />
              {Array(pattern.N_weekly || 1).fill(null).map((_,i) => <View key={i} style={{ flexDirection: 'row' }}>
                {WEEKDAYS.map((d,j) => {
                  const selected = (pattern.weekly ?? [])[i*7 + j];
                  return <Chip
                    key={j}
                    style={{ backgroundColor: selected ? '#9f9' : '#555' }}
                    onPress={() => toggleWeekly(i*7 + j)}
                    selectedColor={selected ? '#000' : '#ddd'}
                    >
                    {d[0].toUpperCase()}
                  </Chip>;
                })}
              </View>)}
            </Dialog.Content>
          </>
          : null}
          {pattern.mode === PATTERN_MODE.monthly
          ? <>
            <Dialog.Title>{'Set schedule'}</Dialog.Title>
            <Dialog.Content>
              {Array(Math.ceil(DAYS_PER_MONTH * 2 / 7)).fill(null).map((_,i) => <View key={i} style={{ flexDirection: 'row' }}>
                {WEEKDAYS.map((d,j) => {
                  const index = i*7 + j;
                  const dayNumber = index > DAYS_PER_MONTH ? index - (2 * DAYS_PER_MONTH + 1) : index + 1;
                  const selected = dummy ? false : getMonthly(dayNumber);
                  const dummy = dayNumber === DAYS_PER_MONTH + 1;
                  return <Chip
                    key={j}
                    style={{
                      backgroundColor: selected ? (dayNumber >= 0 ? '#9f9' : '#f99') : '#555',
                      width: 45,
                      padding: 0,
                      borderRadius: 2,
                      opacity: dummy ? 0 : 1,
                    }}
                    disabled={dummy}
                    onPress={() => toggleMonthly(dayNumber)}
                    selectedColor={selected ? '#000' : '#ddd'}
                    >
                    {dummy ? '-' : dayNumber}
                  </Chip>;
                })}
              </View>)}
            </Dialog.Content>
          </>
          : null}
          <Dialog.Actions>
            <Button onPress={prevPhase}>Previous</Button>
            <Button onPress={updatePattern} disabled={!validateRepeatPattern(pattern)}>Done</Button>
          </Dialog.Actions>
        </>}
      </Dialog>
    </Portal>
  </>
}

export default RepeatPatternInput;