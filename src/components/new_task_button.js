import React from 'react';
import { StyleSheet } from 'react-native';
import { FAB } from 'react-native-paper';
import { useDispatch } from 'react-redux';

import * as appActions from '../actions/index';
import { COLORS } from '../constants/colors';


const NewTaskButton = (props) => {
  const dispatch = useDispatch();
  const createNewTask = () => {
    dispatch(appActions.createNewTask());
  }

  return <>
    <FAB
      icon='plus'
      style={styles.fab}
      onPress={props.onPress || createNewTask}
    />
  </>;
}

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: COLORS.tertiary,
  },
})

export default NewTaskButton;