import React, { useState } from 'react';
import {
  Button,
  Dialog,
  List,
  Portal,
  IconButton,
  Text,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const SpoonInput = (props) => {
  const [open, setOpen] = useState(false);

  const updateSpoons = (spoons) => {
    props.onChange(spoons);
    setOpen(false);
  }

  const hideDialog = () => setOpen(false);

  const MAX_SPOONS = 7;

  return <>
    <List.Item
      title={`${props.spoons} spoons`}
      onPress={() => setOpen(true)}
      right={() => <>
        { Array.from(Array(props.spoons)).map((_, i) =>
          <Icon
            key={i}
            name='silverware-spoon'
            color='white'
            size={24}
            style={{ margin: 3 }}
          />
        ) }
      </>}
      description={props.spoons > 3 ? "Warning: Big task. Consider breaking it down more" : null}
    />
    <Portal>
      <Dialog
        visible={open}
        onDismiss={hideDialog}
        style={{ backgroundColor: '#666' }}
      >
        <Dialog.Title>Choose spoons</Dialog.Title>
        <Dialog.Content>
          <Text>
            {Array.from(Array(MAX_SPOONS)).map((_, i) => {
              const n = i+1;
              return <IconButton
                key={n}
                icon='silverware-spoon'
                onPress={() => updateSpoons(n)}
                style={n > props.spoons ? { opacity: 0.4 } : ''}
              />;
            })}
          </Text>
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={() => updateSpoons(0)}>No spoons</Button>
          <Button onPress={hideDialog}>Cancel</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  </>
}

export default SpoonInput;