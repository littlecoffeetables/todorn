import React, { useState } from "react";
import { View } from "react-native";
import { HelperText, TextInput } from "react-native-paper";


const NumberInput = (props) => {
  const [value, setValue] = useState(''+(props.initialValue ?? props.minValue));

  const isLegalNumber = (val) => (
    typeof val === 'number' &&
    val >= (props.minValue ?? -Infinity) &&
    val <= (props.maxValue ?? Infinity)
  );

  const onChange = (newValue) => {
    setValue(newValue);
    const newNumber = +newValue;
    if (isLegalNumber(newNumber)) {
      props.onChange(newNumber);
    } else {
      props.onChange(NaN);
    }
  }

  return <>
    <TextInput
      error={props.error || !isLegalNumber(value)}
      keyboardType='numeric'
      maxLength={4}
      selectTextOnFocus={true}
      mode='outlined'
      {...props}
      onChange={null}
      onChangeText={onChange}
      value={value}
    />
    {props.helperText ? <HelperText {...(props.helperTextProps || {})}>{props.helperText}</HelperText> : null}
  </>;
}


export default NumberInput;